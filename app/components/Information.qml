import QtQuick 2.12
import Ubuntu.Components 1.3

Page {

    header: PageHeader {
        id:infoHeader
        title: i18n.tr("App Information")

    }

    Column {
        id: aboutColumn
        spacing:units.dp(2)
        width:parent.width

        Label { //An hack to add margin to the column top
            width:parent.width
            height:infoHeader.height *2
        }

        Icon {
          anchors.horizontalCenter: parent.horizontalCenter
          height: Math.min(parent.width/2, parent.height/2)
          width:height
//        For some reason the theme clock-app icon as diffrent background color then the own in our repository.
//        name:"clock-app"
          source:Qt.resolvedUrl("../../../clock-app.svg")
          layer.enabled: true
          layer.effect: UbuntuShapeOverlay {
              relativeRadius: 0.75
           }
        }
        Label {
            width: parent.width
            font.pixelSize: units.gu(3)
            font.bold: true
            color: theme.palette.normal.backgroundText
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Clock App")
        }
        Label {
            width: parent.width
            color: theme.palette.normal.backgroundTertiaryText
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Version %1").arg(Qt.application.version)
        }
        Rectangle {
                id: spacer1
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }
        Repeater {
             anchors {
                top: aboutColumn.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                topMargin: units.gu(2)
             }

             model: [
                 { name: i18n.tr("Get the source"), url: "https://gitlab.com/ubports/apps/clock-app" },
                 { name: i18n.tr("Report issues"), url: "https://gitlab.com/ubports/apps/clock-app/issues" },
                 { name: i18n.tr("Contributors"), url: "https://gitlab.com/ubports/apps/clock-app/graphs/master/" },
                 { name: i18n.tr("Help translate"), url: "https://translate.ubports.com/projects/ubports/clock-app/" }
             ]
             delegate: ListItem {
                divider { visible: false; }
                height: layoutAbout.height
                ListItemLayout {
                    id: layoutAbout
                    title.text : modelData.name
                    ProgressionSlot {color: theme.palette.normal.baseText; }
                }
                onClicked: Qt.openUrlExternally(modelData.url)
            }
        }
    }
}
