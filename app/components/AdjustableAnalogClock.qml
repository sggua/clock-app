import QtQuick 2.12
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0
import QtFeedback 5.0

AnalogMode {
    id:_adjustableAnalogClock
    property var  isMainClock: false
    property bool adjustable: true
    property bool adjusting: false
    property bool suppressAnmation: adjusting
    property bool enableHaptic: false

    signal adjusted(string adjustedTime)

    showSeconds: !adjustable
    partialRotation: !adjustable
    animateRotation: adjustable && !suppressAnmation

    localDateTime: "1970:01:01:00:00:00"
	
	
	//This is an hack to imitate "pressAndHold" with diffrent interval then the default which for some reasion does not seem to work
	//This logic is here to allow swipes overs the face of the clock that  are not intended to adjust its time.
	Timer {
		id:adjustingTimer
		interval:75
		onTriggered: { 
			adjusting = true; 
		}
	}
	
    Image {
        id: hourHandAdjust
        opacity:adjustable && adjusting ? 0.2 : 0
        Behavior on opacity { UbuntuNumberAnimation { duration:UbuntuAnimation.BriskDuration } }
        z: minuteHandAdjust.z + 10
        width: parent.width
        anchors.centerIn: parent

        smooth: true
        source: "../graphics/Hour_Adjust_Circle.png"
        asynchronous: true
        fillMode: Image.PreserveAspectFit
    }

    ClockCircle {
        id:hourCircleLayer
        opacity:adjustable ? 0.66 : 0
        Behavior on opacity { UbuntuNumberAnimation { duration:UbuntuAnimation.BriskDuration } }
        z: 1
        width: parent.width / 2 + units.gu(1.5)
        anchors.centerIn: parent
        isFoldVisible:false
        borderColorTop:"#909E9E9E"
        borderColorBottom:"#B09E9E9E"
        layer.effect: FastBlur {
            radius: units.gu(1)
        }
        layer.enabled: true

    }


    MouseArea {
        id:adjustHourMouseArea
        z: hourHandAdjust.z + 1
        anchors.centerIn: parent
        width:parent.width/2
        height: parent.height/2
        propagateComposedEvents: adjusting
        preventStealing: adjusting

        function updateHours(mouse) {
            if(adjustable && adjusting) {
                var oldLocalDateTime = localDateTime;
                var localPoint = mapToItem(adjustHourMouseArea, mouse.x, mouse.y)
                var minutes = parseInt(localDateTime.split(":")[4]);
                var rot =  1-(Math.PI + Math.atan2(localPoint.x-(width/2),localPoint.y-(height/2))) / (Math.PI*2);
                localDateTime =  "1970:01:01:"+ parseInt(rot * 12) + ":"+minutes+":00";
                if(oldLocalDateTime != localDateTime && enableHaptic )  {
                   Haptics.play();
                }
                adjusted(localDateTime)
            }
        }

        onPressed:  {
           var localPoint = mapToItem(adjustHourMouseArea, mouse.x, mouse.y)
           var tangx = localPoint.x-(width/2);
           var tangy = localPoint.y-(height/2);
            if (!adjustable || Math.abs(Math.sqrt((tangx*tangx)+(tangy*tangy))) > adjustHourMouseArea.width/2 ) {
                 mouse.accepted = false;
            }
            if(!adjusting && adjustable) { adjustingTimer.start(); }

        }

        onReleased: { adjusting = false; adjustingTimer.stop(); }
        onCanceled: { adjusting = false; adjustingTimer.stop(); }

        onWheel : {
                var oldLocalDateTime = localDateTime;
                var minutes = parseInt(localDateTime.split(":")[4]);
                var rot = Math.max(0,Math.min(1,
                                    ((_adjustableAnalogClock.rotationFromTime(3, 30) + (wheel.angleDelta.y > 0 ?30:-30))%360)/360));
                localDateTime =  "1970:01:01:"+ parseInt(rot * 12) + ":"+minutes+":00";
                if(oldLocalDateTime != localDateTime && enableHaptic )  {
                   Haptics.play();
                }
                adjusted(localDateTime)
        }
        onMouseXChanged: if(adjusting) { updateHours(mouse); }
        onMouseYChanged: if(adjusting) { updateHours(mouse); }
    }

    Image {
        id: minuteHandAdjust
        opacity:adjustable && adjusting ? 0.5 : 0
        Behavior on opacity { UbuntuNumberAnimation { duration:UbuntuAnimation.BriskDuration } }
        z: 10
        width: parent.width
        anchors.centerIn: parent

        smooth: true
        source: Theme.name == "Ubuntu.Components.Themes.Ambiance" ? "../graphics/Minute_Adjust_Cirlce.png" : "../graphics/Minute_Adjust_Cirlce_White.png"
        asynchronous: true
        fillMode: Image.PreserveAspectFit
    }

    //Seperate the ohur adjusting area from the minute adjustment area by 3gu
    MouseArea {
        id:adjustHourMinutesSeperator
        z: adjustMinuteMouseArea.z + 1
        anchors.centerIn: parent
        width:adjustHourMouseArea.width + units.gu(3)
        height: adjustHourMouseArea.height + units.gu(3)
        propagateComposedEvents: true
        preventStealing: adjusting

        onPressed:  {
           var localPoint = mapToItem(adjustHourMinutesSeperator, mouse.x, mouse.y)
           var tangx = localPoint.x-(width/2);
           var tangy = localPoint.y-(height/2);
            if (Math.abs(Math.sqrt((tangx*tangx)+(tangy*tangy))) > adjustHourMinutesSeperator.width/2 ) {
                 mouse.accepted = false;
            }
        }
        onReleased: { adjusting = false; adjustingTimer.stop(); }
        onCanceled: { adjusting = false; adjustingTimer.stop(); }
    }

    MouseArea {
        id:adjustMinuteMouseArea
        z:minuteHandAdjust.z + 1
        anchors.fill: parent
        preventStealing: adjusting
        propagateComposedEvents: adjusting


        function updateMinutes(mouse) {
            if(adjustable && adjusting) {
                var oldLocalDateTime = localDateTime;
                var localPoint = mapToItem(adjustMinuteMouseArea, mouse.x, mouse.y)
                var hour = parseInt(localDateTime.split(":")[3]);
                var rot = 1-(Math.PI + Math.atan2(localPoint.x-(width/2),localPoint.y-(parent.height/2))) / (Math.PI*2);
                localDateTime =  "1970:01:01:"+ hour +":"+ parseInt(rot * 60) + ":00";
                if(oldLocalDateTime != localDateTime  && enableHaptic) {
                    Haptics.play();
                }
                adjusted(localDateTime)
            }
        }
        onWheel : {
                var oldLocalDateTime = localDateTime;
                var hour = parseInt(localDateTime.split(":")[3]);
                var rot = Math.max(0,Math.min(1,((_adjustableAnalogClock.rotationFromTime(4, 6) + (wheel.angleDelta.y > 0 ?6:-6) )%360)/360));
                localDateTime =  "1970:01:01:"+ hour +":"+ parseInt(rot * 60) + ":00";
                if(oldLocalDateTime != localDateTime  && enableHaptic) {
                    Haptics.play();
                }
                adjusted(localDateTime)
        }

        onPressed: {
            var localPoint = mapToItem(adjustMinuteMouseArea, mouse.x, mouse.y)
            var tangx = localPoint.x-(width/2);
            var tangy = localPoint.y-(height/2);
            if (!adjustable || Math.abs(Math.sqrt((tangx*tangx)+(tangy*tangy))) > width/2 ) {
                 mouse.accepted = false;
            }
	    if(!adjusting && adjustable) {  adjustingTimer.start(); }
        }

        onReleased: { adjusting = false; adjustingTimer.stop(); }
        onCanceled: { adjusting = false; adjustingTimer.stop(); }

        onMouseXChanged: if(adjusting) { updateMinutes(mouse); }
        onMouseYChanged: if(adjusting) { updateMinutes(mouse); }
    }

    function setTime(time) {
        localDateTime = "1970:01:01:" + time.getUTCHours() + ":" + time.getUTCMinutes() + ":" + time.getSeconds();
    }

    function getTime() {
        if(!localDateTime) { return new Date(0); }

        var tmpTime = new Date(Date.now());
        return new Date( tmpTime.setHours( tmpTime.getHours() + parseInt(localDateTime.split(":")[3]),
                                           tmpTime.getMinutes() + parseInt(localDateTime.split(":")[4]),
                                           tmpTime.getSeconds() + parseInt(localDateTime.split(":")[5]) ) );
    }
}
