/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Clock App
 *
 * Ubuntu Clock App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Clock App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtSystemInfo 5.0
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0

import "upstreamcomponents"
import "alarm"
import "clock"
import "stopwatch"
import "components"

Page {
    id: _mainPage
    objectName: "mainPage"

    // String with not localized date and time in format "yyyy:MM:dd:hh:mm:ss", eg.: "2016:10:05:16:10:15"
    property string notLocalizedDateTimeString

    // String with localized time, eg.: "4:10 PM"
    property string localizedTimeString

    // String with localized date, eg.: "Thursday, 17 September 2016"
    property string localizedDateString

    // Property to keep track of an app cold start status
    property alias isColdStart: clockPage.isColdStart

    // Property to check if the clock page is currently visible
    property bool isClockPage: listview.currentIndex === 0

    // Clock App Alarm Model Reference Variable
    property var alarmModel

    function reloadBottomEdge() {
        bottomEdgeLoader.reload()
    }

    Timer {
        id: hideBottomEdgeHintTimer
        interval: 5000
        onTriggered: bottomEdgeLoader.item.hint.status = BottomEdgeHint.Inactive
    }

    Loader {
        id: bottomEdgeLoader
        asynchronous: true

        property bool openBottomEdgeOnLoad: false
        property var openUrlQuery: null

        onLoaded: {
            item.alarmModel = Qt.binding( function () { return  _mainPage.alarmModel } );
            item.hint.visible = Qt.binding( function () { return _mainPage.isClockPage } );
            if(openBottomEdgeOnLoad) {
                item.commit()
                bottomEdgeLoader.handleQuery(openUrlQuery);
                openBottomEdgeOnLoad = false
            }
            hideBottomEdgeHintTimer.start();
        }
        function reload() {
           setSource("components/AlarmBottomEdge.qml", {
                                               "id":"bottomEdge",
                                               "objectName": "bottomEdge",
                                               "parent": _mainPage,
                                               "pageStack": mainStack,
                                               "hint.objectName": "bottomEdgeHint"
                                            })
        }
        Component.onCompleted: reload();
        
        //-------- Functions ---------
        function handleQuery(query) {
            if(Object.keys(query).length !== 0 ) {
                var alarm = query['date'] ? 
                                alarmUtils.findAlarmByDate(new Date(intval(query['date'])), bottomEdgeLoader.item.alarmModel) :
                                alarmUtils.findAlarmByMessage(query['label'] ? query['label'] : query['message'], bottomEdgeLoader.item.alarmModel) ;

                if(alarm) {
                        console.log("found Alarm".alarm);
                        if(activeTimers.isAlarmATimerAlarm(alarm)) {
                            listview.moveToTimerPage();
                        } else {
                            bottomEdgeLoader.item.editAlarm(alarm);
                        }
                } else {
                    console.log("couldn't find alarm for query :"+JSON.stringify(query))
                }
            }
        }
    }

    ClockAppUtils {
        id:clockAppUtils
    }
    
    AlarmUtils {
        id: alarmUtils
    }

    ScreenSaver {
        // Disable screen dimming/off when stopwatch is running
        screenSaverEnabled: stopwatchPageLoader.item && !stopwatchPageLoader.item.isRunning
    }

    VisualItemModel {
        id: navigationModel
        ClockPage {
            id: clockPage
            anchors.bottomMargin: units.gu(4)
            notLocalizedClockTimeString: _mainPage.notLocalizedDateTimeString
            localizedClockTimeString: _mainPage.localizedTimeString
            localizedClockDateString: _mainPage.localizedDateString
            width: clockApp.width
            height: listview.height -units.gu(4)
            onStartupAnimationEnd: {
                stopwatchPageLoader.setSource("stopwatch/StopwatchPage.qml" ,{
                                                                     "notLocalizedClockTimeString": _mainPage.notLocalizedDateTimeString,
                                                                     "localizedClockTimeString": _mainPage.localizedTimeString,
                                                                     "localizedClockDateString": _mainPage.localizedDateString,
                                                                     "width": clockApp.width,
                                                                     "height": listview.height});
                timerPageLoader.setSource("timer/TimerPage.qml" ,{
                                                                 "width": clockApp.width,
                                                                 "height": listview.height });
            }

        }

        Loader {
            id:stopwatchPageLoader
            asynchronous: true
            width: clockApp.width
            height: listview.height- units.gu(4)
            anchors.bottomMargin: units.gu(4)
            onLoaded: {
                if (this.item.isRunning) {
                    listview.moveToStopwatchPage()
                }
            }
        }

        Loader {
            id:timerPageLoader
            asynchronous: true
            active: alarmModel !== null || timerPageLoader.item;
            width: clockApp.width
            height: Math.max(listview.height , units.gu(40)) - units.gu(4)
            anchors.bottomMargin: units.gu(4)
           // anchors.bottom:parent.bottom
            onLoaded: {
                item.alarmModel = Qt.binding( function () { return  _mainPage.alarmModel } )
               // item.anchors.bottom = Qt.binding( function () { return parent.bottom } )
                if (this.item.isRunning) {
                    listview.moveToTimerPage()
                    this.item.collapse()
                }
            }
        }
    }

    header: PageHeader {
        visible:true
        StyleHints {
            backgroundColor: "transparent"
            dividerColor: "transparent"
        }
        contents: NavigationRow {
            id: headerNavRow
            anchors {
               fill:parent
               leftMargin: mainTrailingActions.width
            }
        }

        trailingActionBar {
            id:mainTrailingActions
            actions : [
                Action {
                    id: settingsIcon
                    objectName: "settingsIcon"
                    text: i18n.tr("Settings")
                    iconName: "settings"
                    onTriggered: {
                        mainStack.push(Qt.resolvedUrl("./alarm/AlarmSettingsPage.qml"))
                    }
                }
            ]
            numberOfSlots: 1
        }
    }



    ListView {
        id: listview
        objectName: "pageListView"

        // Property required only in autopilot to check if listitem has finished moving
        property alias isMoving: moveAnimation.running

        function moveToStopwatchPage() {
            moveAnimation.moveTo(listview.originX + listview.width)
            listview.currentIndex = 1
        }

        function moveToTimerPage() {
            moveAnimation.moveTo(listview.originX + listview.width*2)
            listview.currentIndex = 2
        }

        function moveToClockPage() {
            moveAnimation.moveTo(listview.originX)
            listview.currentIndex = 0
        }

        function updateListViewCurrentIndex() {
               listview.currentIndex = listview.indexAt(listview.contentX,0);
        }

        onMovementEnded: updateListViewCurrentIndex();
        onCurrentIndexChanged: listview.interactive = true

        UbuntuNumberAnimation {
            id: moveAnimation
            objectName: "pageListViewAnimation"

            target: listview
            property: "contentX"
            function moveTo(newContentX) {
                from = listview.contentX
                to = newContentX
                start()
            }
            onStopped: {
                listview.positionViewAtIndex(listview.currentIndex,ListView.Contain);
            }
        }

        // Show the stopwatch page on app startup if it is running
        Component.onCompleted: {
            if (stopwatchPageLoader.item && stopwatchPageLoader.item.isRunning) {
                moveToStopwatchPage()
            }
        }

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: units.gu(4)
        }

        model: navigationModel
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        flickDeceleration:width/4
        maximumFlickVelocity: width*10
        interactive: true
    }

    BottomSwipeArea {
        height:units.gu(4)
        anchors {
            left:parent.left
            right:parent.right
            bottom: parent.bottom
        }
        z:10
        visible: !Qt.inputMethod.visible
    }
    
    // ----------------------------------- Functions -------------------------------
    
    function showAlarm(url) {
        console.log("Showing alarm for URL :" + url);
        var query = clockAppUtils.getUrlQueryArguments(url);

        if(bottomEdgeLoader.item) {
            bottomEdgeLoader.item.commit();
            bottomEdgeLoader.handleQuery(query);
        } else {
            bottomEdgeLoader.openBottomEdgeOnLoad = true;
            bottomEdgeLoader.openUrlQuery = query;
        }
    }
    
    
    function handleURLPage(url) {
        var pages = {
            "^clock://" : listview.moveToClockPage,
            "^stopwatch://" : listview.moveToStopwatchPage,
            "^timer://" : listview.moveToTimerPage
        }
        console.log("Handling URL :" + url);
        for(var match in pages) {
            if (url.match(new RegExp(match))) {
                pages[match]();
            }
        }
    }
}
